#!/bin/bash
# Calls profiles_hdf5_loop several times
#
# Arguments are Simulation Ion S/N (or 'inf' for no noise)

profiles_hdf5_loop ${1} short 0.010 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.020 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.030 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.041 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.051 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.061 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.072 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.083 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.094 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.105 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.116 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.127 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.138 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.149 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.161 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.173 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.184 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.196 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.208 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.220 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.232 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.257 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.270 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.282 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.295 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.308 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.321 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.334 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.348 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.361 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.375 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.389 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.403 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.417 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.431 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.445 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.459 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.489 1 100 ${2} ${3}
profiles_hdf5_loop ${1} short 0.504 1 100 ${2} ${3}
