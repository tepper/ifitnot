#!/bin/bash
# Calls multi_spec_fit.sh several times

#./multi_spec_fit.sh DEFAULT_L100N512_ibf0.5_ZOx2 30
#./multi_spec_fit.sh MILL_L100N512 30
#./multi_spec_fit.sh NOAGB_NOSNIa_L100N512 30
#./multi_spec_fit.sh NOSN_L100N512 30
#./multi_spec_fit.sh NOZCOOL_L100N512 30
#./multi_spec_fit.sh REF_L025N128 30
#./multi_spec_fit.sh REF_L050N256 30
#./multi_spec_fit.sh REF_L050N512 30
#./multi_spec_fit.sh REF_L100N256 30
#./multi_spec_fit.sh REF_L100N512 30
#./multi_spec_fit.sh REF_L100N512_metgrad_1 30
#./multi_spec_fit.sh REF_L100N512_metgrad_2 30
#./multi_spec_fit.sh REF_L100N512_metgrad_3 30
#./multi_spec_fit.sh REF_L100N512_metgrad_4 30
#./multi_spec_fit.sh REF_L100N512_metgrad_Aguirre_ea_08 30
#./multi_spec_fit.sh REF_L100N512_metgrad_Aguirre_ea_08_1sigma 30
#./multi_spec_fit.sh WDENS_L100N512 30
#./multi_spec_fit.sh WML1V848_L100N512 30
#./multi_spec_fit.sh WML4_L100N512 30
#./multi_spec_fit.sh WTHERMAL_WMAP5_L100N512 30
#./multi_spec_fit.sh WVCIRC_L100N512 30
