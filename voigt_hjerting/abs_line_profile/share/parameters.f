!	(C) Thorsten Tepper Garcia 2009
!
!	CONTAINS THE VARIABLES AND PARAMETERS NEEDED TO RUN
!	abs_line_profile.f
!

!	=============================================================
!	Physical parameters (cgs UNITS)

    	double precision, parameter :: sigma0=6.3D-18
	double precision, parameter :: CLIGHT=2.9979D+10
	double precision, parameter :: ERADIUS=2.817D-13
	double precision, parameter :: PI=3.141593D0
	double precision, parameter :: ECHARGE=1.602D-19
	double precision, parameter :: EMASS=9.109D-34
     	
	double precision, parameter :: resolution = 0.001 !in Angstroem
