*	----------------------------------------------------------------
*	Voigt-Profile approximation to model Lyman absorption lines
*	----------------------------------------------------------------
*	(C) Thorsten Tepper Garcia 2006
*	Based on the approximation to the Voigt-Hjerting function
*	published in:
*	Tepper Garcia, Thorsten
*	Monthly Notices of the Royal Astronomical Society; Volume 369, 
*	Issue 4, Page 2025
*	Please refer to the author when using this approximation.

	PROGRAM absmaks

*	Lyman-Series Absorption Lines are calculated according to the
*	approximation for a<<1 (where a is the damping parameter in the
*	Voigt-Hjerting function H(a,x)) given by (Tepper Garcia 2006)
*	
*	H(a,x) = h0-a/sqrt(PI)/x^2*(h0^2*(4x^4 + 7x^2 + 4 + Q) - 1 - Q)
*	with h0=exp(-x^2), Q=1.5 x^(-2)
*
*	The arguments of the program are the Doppler parameter b (in
*	units km/s) and the logarithmic column density (in units of
*	log10(cm^{-2})). The output of the program is a spectrum with
*	Voigt-profileas a function of wavelength for the first 31 
*	transitions of theLyman series. All physical and other needed 
*	parameters are included in the file 'parameter.h'. The wavelength 
*	range is [900.0,1400] Angstroem, with a resolution of 0.01 A. The
*	datablock corresponding to each line is separated by a '#'. The
*	output is standard output and can be redirected to any desired
*	file.
*	The resolution, number of lines (transitions), etc. can be
*	modified if needed. Note however, that nlines <= 31!
*	Furthermore, the author is not responsible for any modifications
*	done to this code in its original form.
*	The line parameters (central wavelength of the transition, osc.
*	strength, and gamma-value) are contained in the file
*	'lyman_series.dat', and were taken from Morton's Compilation
*	(see Tepper-Garcia 2006 for corresponding reference).
*	Please report any bugs or problems to:
*	tepper@astro.physik.uni-goettingen.de
*	----------------------------------------------------------------
*	Variables declaration
*	----------------------------------------------------------------
        implicit none
	
	double precision sigma0,CLIGHT,ERADIUS,PI,ECHARGE,EMASS
     	parameter (sigma0=6.3D-18)
	parameter (CLIGHT=2.9979D+10)
	parameter (ERADIUS=2.817D-13)
	parameter (PI=3.141593D0)
	parameter (ECHARGE=1.602D-19,EMASS=9.109D-34)
     	
	double precision resolution
	parameter (resolution = 0.01)
	
	integer nlines, line_num, points
	parameter (nlines = 25, points = 122000)

	intrinsic DEXP, DSQRT
     	double precision central_wl(nlines), wavelength(points),
     +	fvalue(nlines), gamma(nlines), flux(points)
     	double precision b, logNHI, NHI, dopp_width, damping, alpha, 
     +	tau_neg, sqrtPI, lylimit
	double precision voigt_hjerting
	character*5 dummy
	integer argcount
	integer i,j

*	----------------------------------------------------------------
*	Getting arguments
*	----------------------------------------------------------------
	argcount = IArgC()
      	if (argcount .lt. 3) then
         print *,'USAGE: absmask <No.of trans.> <b (km/s)> <log10NHI (log10(cm^{-2})>'
	 STOP
      	end if
	
	call GetArg(1,dummy)
	read (dummy,'(I2)') line_num

	call GetArg(2,dummy)
	read (dummy,'(F5.2)') b

	call GetArg(3,dummy)
	read (dummy,'(F5.2)') logNHI

*	----------------------------------------------------------------
*	Reading Lyman Series line-parameters
*	----------------------------------------------------------------
*	Reads in wavelength (in Angstroem), oscillator strength and
*	gamma-value (in Hz) for the first nlines=31 transitions of the
*	Lyman Series

 116    FORMAT(T15, F9.4, 1X, F8.6, 2X, 1pE7.3)
	open(70, FILE = 'lyman_series.dat')
	 read(70,*)
	 read(70,*)   
	 do i=1,nlines
	  read(70,116) central_wl(i), fvalue(i), gamma(i)
	 end do	 
	close(70)

*	----------------------------------------------------------------
*	Calculating optical depth as a function of wavelength
*	----------------------------------------------------------------
 100	format(a,f9.3,d14.6,f9.3,d14.6,d14.6,$)
	do i=1,points
	 flux(i) = 1.0d0
	end do

	sqrtPI = DSQRT(PI)
	NHI = 10.0D0**logNHI				! in cm^{-2}
	
	do i=1,line_num

*	The following parameters are in cgs units      

	dopp_width = (central_wl(i)*b/CLIGHT)*1.0D-3
	damping = (central_wl(i)*gamma(i)/(4.0D0*PI*b))*1.0D-13
	alpha = (sqrtPI*ERADIUS*NHI*(central_wl(i)*central_wl(i))*
     1  fvalue(i)/dopp_width)*1.0D-16
	
	 do j = 1, points

	  wavelength(j) = (j-1)*resolution	       ! in Angstroem
	
	  tau_neg = (-alpha)*
     1	  voigt_hjerting(damping,b,central_wl(i),wavelength(j))
	  flux(j) = flux(j)*DEXP(tau_neg)
	 end do
	

	end do
	 
	do i=1,points 
	  write(6,*) wavelength(i), flux(i)*lylimit(NHI,wavelength(i))
	end do
	
*	----------------------------------------------------------------
	END PROGRAM
*	----------------------------------------------------------------

*=======================================================================
	function voigt_hjerting(a,b,cwl,wl)
*	Returns the value of the Voigt-Hjerting function H(a,x) as a
*	function of wavelength for a given a-parameter, Doppler
*	parameter (in km/s), and central wavelength (in Angstroem) of
*	the corresponding transition.

	intrinsic DSQRT
	double precision voigt_hjerting
	double precision a, b, dopp, cwl, wl
	double precision x,x2,h0,Q,PI,CLIGHT
	parameter (PI=3.141593D0,CLIGHT=2.9979D+10)
	
	dopp = (cwl*b/CLIGHT)*1.0D-3
	x = ((wl-cwl)/dopp)*1.0D-8
	x2 = x*x
	h0 = DEXP(-x2)
	Q = 1.5D0/x2

	if (DABS(x).GT.4.0D-4) then		!arbitrarily set

	 voigt_hjerting = h0 - a/DSQRT(PI)/x2*
     1	 (h0*h0*(4.0D0*x2*x2 + 7.0D0*x2 + 4.0D0 + Q) - 1.0D0 - Q)

	else					!limes for x -> 0

	 voigt_hjerting = 1.0d0 - 2.0D0*a/DSQRT(PI)

	endif
*	voigt_hjerting = h0
	return
*	----------------------------------------------------------------
	end function
*	----------------------------------------------------------------
*=======================================================================

*	----------------------------------------------------------------
*	Lyman Limit absorption
*	----------------------------------------------------------------
	
	function lylimit(aNHI,wl)

*	Implementing attenuation for lambda < alylim (at the correct 
*	epoch of absorption).

*	----------------------------------------------------------------
*	Declaring variables
*	----------------------------------------------------------------
*	GAUNT-Factor not yet included!
        
	implicit none
	double precision lylimit
	double precision lyliminv,aNHI,wl
	double precision alylim,sigma0
	parameter (alylim=911.75d0,sigma0=6.3d-18)
     	double precision tauneg, ratio
*	---------------------------------------------------------------
*	Subroutine start
*	----------------------------------------------------------------

*	Redshifting 1/(Ly-lim)

	lyliminv=1/alylim

	if (wl .LE. alylim) then
	 ratio = wl*lyliminv
	 tauneg = (-1.0d0)*aNHI*sigma0*ratio*ratio*ratio
	 lylimit = DEXP(tauneg)
     	
	else
	 lylimit = 1.0d0
	
	end if
	
	return
*	----------------------------------------------------------------
	end function
*	----------------------------------------------------------------

*=======================================================================
