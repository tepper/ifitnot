*	----------------------------------------------------------------
*	Voigt-Profile approximation to model Lyman absorption lines
*	----------------------------------------------------------------
*	(C) Thorsten Tepper Garcia 2006
*	Based on the approximation to the Voigt-Hjerting function
*	published in:
*	Tepper Garcia, Thorsten
*	Monthly Notices of the Royal Astronomical Society; Volume 369, 
*	Issue 4, Page 2025
*	Please refer to the author when using this approximation.

	PROGRAM tables

*	Lyman-Series Absorption Lines are calculated according to the
*	approximation for a<<1 (where a is the damping parameter in the
*	Voigt-Hjerting function H(a,x)) given by (Tepper Garcia 2006)
*	
*	H(a,x) = h0-a/sqrt(PI)/x^2*(h0^2*(4x^4 + 7x^2 + 4 + Q) - 1 - Q)
*	with h0=exp(-x^2), Q=1.5 x^(-2)
*
*	The arguments of the program are the Doppler parameter b (in
*	units km/s) and the logarithmic column density (in units of
*	log10(cm^{-2})). The output of the program is a table with
*	physical parameters for the first 31 transitions of the
*	Lyman series. All physical and other needed parameters are
*	included in the file 'parameter.h'The wavelength range is
*	[900.0,1400] Angstroem, with a resolution of 0.01 A. The
*	datablock corresponding to each line is separated by a '#'. The
*	output is standard output and can be redirected to any desired
*	file.
*	The resolution, number of lines (transitions), etc. can be
*	modified if needed. Note however, that nlines <= 31!
*	Furthermore, the author is not responsible for any modifications
*	done to this code in its original form.
*	The line parameters (central wavelength of the transition, osc.
*	strength, and gamma-value) are contained in the file
*	'lyman_series.dat', and were taken from Morton's Compilation
*	(see Tepper-Garcia 2006 for corresponding reference).
*	Please report any bugs or problems to:
*	tepper@astro.physik.uni-goettingen.de
*	----------------------------------------------------------------
*	Variables declaration
*	----------------------------------------------------------------
        implicit none
	include'parameters.f'
	intrinsic DEXP, DSQRT
     	double precision central_wl(nlines),fvalue(nlines),gamma(nlines)
     	double precision b, logNHI, NHI, dopp_width, damping, alpha, 
     +	sqrtPI
	character*5 dummy, ch_b*4, ch_line*2
	integer argcount
	integer i,j

*	----------------------------------------------------------------
*	Getting arguments
*	----------------------------------------------------------------
	argcount = IArgC()
      	if (argcount .lt. 1) then
         print *,'USAGE: profile <b (km/s)>'
	 STOP
      	end if
	
	call GetArg(1,dummy)
	read (dummy,'(F5.2)') b

*	----------------------------------------------------------------
*	Reading Lyman Series line-parameters
*	----------------------------------------------------------------
*	Reads in wavelength (in Angstroem), oscillator strength and
*	gamma-value (in Hz) for the first nlines=31 transitions of the
*	Lyman Series

 116    FORMAT(T15, F9.4, 1X, F8.6, 2X, 1pE7.3)
	open(70, FILE = '../lyman_series.dat')
	 read(70,*)
	 read(70,*)   
	 do i=1,nlines
	  read(70,116) central_wl(i), fvalue(i), gamma(i)
	 end do	 
	close(70)

*	----------------------------------------------------------------
*	Calculating optical depth as a function of wavelength
*	----------------------------------------------------------------
	sqrtPI = DSQRT(PI)
	
	do i=1,nlines

	 write(ch_b,'(f4.1)') b
	 write(ch_line,'(I2)') i
	
	 open(80,file='table_'//ch_line//'_'//ch_b//'.dat')
	 write(80,*) '#', central_wl(i),b
	 
	 do j=1,20
	
	  NHI = 10.0D0**(logNHImin+j*0.5)			! in cm^{-2}
	  print*, NHI
*	  The following parameters are in cgs units	

	  dopp_width = (central_wl(i)*b/CLIGHT)*1.0D-3
	  damping = (central_wl(i)*gamma(i)/(4.0D0*PI*b))*1.0D-13
	  alpha = (sqrtPI*ERADIUS*NHI*(central_wl(i)*central_wl(i))*
     1    fvalue(i)/dopp_width)*1.0D-16
	  
	  write(80,*) logNHImin+j*0.5,dopp_width, damping,alpha
	 
	 end do
	 
	 close(80)
	
	end do
	
*	----------------------------------------------------------------
	END PROGRAM
*	----------------------------------------------------------------
